<?php
  // Create connection
  $conn = new mysqli("localhost","root","1234","st");

  // Check connection
  if ($conn->connect_error)
  {
      die("Connection failed: " . $conn->connect_error);
  }

  $q = $_REQUEST["q"];
  $sql = "SELECT Zipcode, District, Place FROM locations WHERE Zipcode=$q";
  $result = $conn->query($sql);

  if ($result->num_rows > 0)
  {
    // output data of each row
    echo "<option selected> </option>";
    while($row = $result->fetch_assoc())
    {
      $zip=$row["Zipcode"];
      $district=$row["District"];
      $place=$row["Place"];

      echo "<option>$place</option>";

    }
  }
  else
  {
    echo 0;
  }

  $conn->close();

?>

<?php
  // Create connection
  $conn = new mysqli("localhost","root","1234","st");

  // Check connection
  if ($conn->connect_error)
  {
      die("Connection failed: " . $conn->connect_error);
  }
  echo "Connected successfully<br>";
  //To ensure that the HTML form has used "method=POST"
  if ($_SERVER["REQUEST_METHOD"] == "POST")
  {
    //Intialise the variables with the incoming values from the form
    $name = $_POST["name"];

    //Check the value of Zip, Latitude & Longitude & set it to 0, if they're empty
    //Zipcode
    if (empty($_POST["zip"])) {
      $zip = 0;
    }
    else {
      $zip = $_POST["zip"];
    }
    //Latitude
    if (empty($_POST["lat"])) {
      $lat = 0;
    }
    else {
      $lat = $_POST["lat"];
    }
    //Longitude
    if (empty($_POST["lon"])) {
      $lon = 0;
    }
    else {
      $lon = $_POST["lon"];
    }

    switch ($_POST["Type"])
    {
      case '1': $type = "Tree Plantation";
                break;
      case '2': $type = "Awareness Camp/Rally";
                break;
      case '3': $type = "Protest against Tree Cutting";
                break;
      default : echo "Some error has occured while choosing the 'Type'";
                break;
    }
    $sub = $_POST["subject"];
    $msg = $_POST["msg"];
    $place = $_POST["place"];

    //Image Upload
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["eventImage"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"]))
    {
        $check = getimagesize($_FILES["eventImage"]["tmp_name"]);
        if($check !== false)
        {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        }
        else
        {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0)
    {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    }
    else
    {
        if (move_uploaded_file($_FILES["eventImage"]["tmp_name"], $target_file))
        {
            echo "The file ". basename( $_FILES["eventImage"]["name"]). " has been uploaded.";
        }
        else
        {
            echo "Sorry, there was an error uploading your file.";
        }
    }

    //Query to store the data provided by the user
    $sql = "INSERT INTO post_data(Name,Zip,Latitude,Longitude,Place,Type,Image,Subject,Message,Verif_Stat,V_ID) VALUES('$name',$zip,$lat,$lon,'$place','$type','$target_file','$sub','$msg',0,0)";

    if($conn->query($sql)===TRUE)
    {
      echo "New record created";
      header("Location: index.html");
    }
    else
    {
      echo "ERROR : ".$sql."<br>".$conn->error;
    }
  }

  $conn->close();

?>

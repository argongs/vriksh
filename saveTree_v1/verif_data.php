<?php
  // Create connection
  $conn = new mysqli("localhost","root","1234","st");

  // Check connection
  if ($conn->connect_error)
  {
      die("Connection failed: " . $conn->connect_error);
  }

  //To ensure that the HTML form has used "method=POST"
  $sql = "SELECT R_ID, Time, Place, Type FROM post_data WHERE Verif_Stat=1";
  $result = $conn->query($sql);
  $i=0;

  if ($result->num_rows > 0)
  {
    // output data of each row
    while($row = $result->fetch_assoc() and $i<4)
    {
      $id=$row["R_ID"];
      $time=$row["Time"];
      $type=$row["Type"];
      $place=$row["Place"];
      $img=$row["Image"];
      //echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
      if($i==0)
      {
        echo "<article class=\"one_quarter first\">
        <time class=\"block font-xs\" datetime=\"2045-04-06\">$time</time>
        <h4 class=\"nospace font-x1\">$place</h4>
        <p class=\"btmspace-30\">$type</p>
        <a href=\"#\"><img class=\"btmspace-30\" src=\"$img\" alt=\"\"></a>
        <footer><a onclick=\"expand($id)\">Read More &raquo;</a></footer>
        </article>";
      }
      else {
        echo "<article class=\"one_quarter\">
        <time class=\"block font-xs\" datetime=\"2045-04-06\">$time</time>
        <h4 class=\"nospace font-x1\">$place</h4>
        <p class=\"btmspace-30\">$type</p>
        <a href=\"#\"><img class=\"btmspace-30\" src=\"$img\" alt=\"\"></a>
        <footer><a onclick=\"expand($id)\">Read More &raquo;</a></footer>
        </article>";
      }

      $i=$i+1;
    }
  }
  else
  {
    echo "0 results";
  }

  $conn->close();

?>
//

<?php

  //Create connection
  $conn = new mysqli("localhost","root","1234","st");
  $selDist = $_REQUEST["q"];
  $length = strlen($selDist);
  $last = substr($selDist,$length-1,1);
  // Check connection
  if ($conn->connect_error)
  {
      die("Connection failed: " . $conn->connect_error);
  }

  if ($last!="5")
  {
    if ($selDist=="0")
    {
      $sql_dist = "SELECT DISTINCT District FROM locations ORDER BY District ASC ";
      $result = $conn->query($sql_dist);

      if ($result->num_rows > 0)
      {
        // output data of each row
        echo "<option selected> </option>";
        while($row = $result->fetch_assoc())
        {
          $rd = $row["District"];
          echo "<option value=\"{$rd}\">{$rd}</option>";
        }
      }
    }
    else
    {
      $sql_place = "SELECT Place FROM locations WHERE District='$selDist' ORDER BY Place ASC ";
      $result = $conn->query($sql_place);
      if ($result->num_rows > 0)
      {
        // output data of each row
        echo "<option selected> </option>";
        while($row = $result->fetch_assoc())
        {
          $rp = $row["Place"];
          echo "<option value=\"{$rp}\">{$rp}</option>";
          //echo "<option value=\"{$rp}\">{$rp}</option>";
        }
      }

    }
  }
  else
  {
        $un = substr($selDist,0,$length-1);
        $sql = "SELECT userName FROM register WHERE userName='$un'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0)
        {
          echo "Sorry, this username has been taken. Try a new one!";
        }
        else
        {
          echo "This username awaits you!";
        }
  }

  $conn->close();


?>

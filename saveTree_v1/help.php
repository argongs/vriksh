<?php
  // Create connection
  $conn = new mysqli("localhost","root","1234","st");

  // Check connection
  if ($conn->connect_error)
  {
      die("Connection failed: " . $conn->connect_error);
  }

  $q = $_REQUEST["q"];
  $sql = "SELECT * FROM post_data WHERE R_ID=$q AND Verif_Stat=1";
  $result = $conn->query($sql);

  if ($result->num_rows > 0)
  {
    // output data of each row
    while($row = $result->fetch_assoc())
    {
      $id=$row["R_ID"];
      $name=$row["Name"];
      $time=$row["Time"];
      $type=$row["Type"];
      $place=$row["Place"];
      $sub=$row["Subject"];
      $msg=$row["Message"];
      //echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";

      echo "<section class=\"hoc container clear\">
        <!-- ################################################################################################ -->
        <div class=\"btmspace-80 center\">
          <h3 class=\"nospace\">$type</h3>
          <p class=\"nospace\">$sub</p>
        </div>
        <div class=\"one_third first gallery\">
          <img src=\"images/demo/user.png\" width=\"12px\" height=\"12px\">
          $name
        </div>
        <div class=\"one_third gallery\" align=\"right\">
        <img src=\"images/demo/calender.png\" width=\"12px\" height=\"12px\">
         $time
        </div>
        <!-- ################################################################################################ -->
      </section>";

    }
  }
  else
  {
    echo "0 results";
  }

  $conn->close();

?>

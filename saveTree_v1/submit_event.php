<?php

  try
  {
    session_start();
    // Create connection
    $conn = new mysqli("localhost","root","1234","st");
    // Check connection
    if ($conn->connect_error)
    {
        die("Connection failed: " . $conn->connect_error);
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
      //Intialise the variables with the incoming values from the form
      $checklist = $_POST["checklist"];
      $ID = $_SESSION["id"];
      if (!empty($checklist))
      {
        foreach ($checklist as $item)
        {
          $sql_verif = "UPDATE post_data SET Verif_Stat=1, V_ID=$ID WHERE R_ID=$item";
          if(!$conn->query($sql_verif))
          {
            echo "ERROR : ".$sql."<br>".$conn->error;
            break;
          }
        }
      }
    }
    $conn->close();
    header("Location: voindex.html");
  }
  catch (Throwable $e)
  {
    $trace = $e->getTrace();
    echo $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine().' called from '.$trace[0]['file'].' on line '.$trace[0]['line'];
  }

?>
